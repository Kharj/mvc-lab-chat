﻿// <copyright file="TableNameConvention.cs" company="GoGoId">
// GoGoId copyright, 2013
// </copyright>
// <author>Natalie Vegerina nvegerina@infostroy.com.ua</author>

namespace Data.Conventions
{
    using System;
    using System.Collections.Generic;
    using FluentNHibernate.Conventions;
    using FluentNHibernate.Conventions.Instances;
    using System.Globalization;

    public class TableNameConvention : IClassConvention
    {
        public void Apply(IClassInstance instance)
        {
            string typeName = instance.EntityType.Name;
            instance.Table(Inflector.Inflector.Pluralize(typeName));
        }
    }
}
