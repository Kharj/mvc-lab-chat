﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class User : Entity
    {
        public virtual int Id { get; set; }
        public virtual string Nickname { get; set; }
        public virtual string Name { get; set; }
        public virtual string Password { get; set; }
        public virtual DateTime RegistrationDatetime { get; set; }
        public virtual string Email { get; set; }
        public virtual string Phone { get; set; }
    }
}
