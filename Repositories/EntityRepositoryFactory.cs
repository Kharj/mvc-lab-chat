﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data;
using Domain;
using NHibernate;

namespace Repositories
{
    public class EntityRepositoryFactory : IDisposable
    {
        private ISession session;

        public EntityRepositoryFactory()
        {
            this.session = this.GetSession();
        }

        private ISession GetSession()
        {
            return DbSessionFactory.Instance.OpenSession();
        }

        public void CloseSession()
        {
            if (this.session != null && this.session.IsOpen)
            {
                this.session.Close();
            }
            this.session = null;
        }

        public EntityRepository<T> GetRepository<T>()
            where T : Entity
        {
            if (typeof(T) == typeof(User))
            {
                return new UserRepository(this.session) as EntityRepository<T>;
            }

           /* if (typeof(T) == typeof(Vehicle))
            {
                return new VehicleRepository(this.session) as EntityRepository<T>;
            }*/
            return new EntityRepository<T>(this.session);
        }

        #region IDisposable Members

        public void Dispose()
        {
            this.CloseSession();
        }

        #endregion
    }
}
