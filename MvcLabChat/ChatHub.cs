﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using System.Threading.Tasks;

namespace MvcLabChat
{
    public class ChatHub : Hub
    {
        protected List<String> usernames = new List<String>();
        public void User(string name)
        {
            usernames.Add(name);
            Clients.All.loadusers(usernames);
        }
        public void Send(string name, string message)
        {
            // Call the addNewMessageToPage method to update clients. 
            Clients.All.addNewMessageToPage(name, message);
        }
    }
}