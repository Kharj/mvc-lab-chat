﻿// <copyright file="IdConvention.cs" company="GoGoId">
// GoGoId copyright, 2013
// </copyright>
// <author>Natalie Vegerina nvegerina@infostroy.com.ua</author>

namespace Data.Conventions
{
    using System;
    using System.Collections.Generic;
    using FluentNHibernate.Conventions;
    using NHibernate.Id;

    public class IdConvention : IIdConvention
    {
        public void Apply(FluentNHibernate.Conventions.Instances.IIdentityInstance instance)
        {
            instance.Column("Id");
            instance.GeneratedBy.Native();
        }
    }
}
