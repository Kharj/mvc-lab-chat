﻿// <copyright file="OneToManyConvention.cs" company="GoGoId">
// GoGoId copyright, 2013
// </copyright>
// <author>Natalie Vegerina nvegerina@infostroy.com.ua</author>

namespace Data.Conventions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using FluentNHibernate.Conventions.Instances;
    using FluentNHibernate.Conventions;
    using System.Collections;

    public class OneToManyConvention : IHasManyConvention
    {
        public virtual void Apply(IOneToManyCollectionInstance instance)
        {
            instance.Cascade.None();
            instance.Inverse();
            //string customKey = null;
            instance.Key.Column(instance.Member.DeclaringType.Name + "Id");
            instance.LazyLoad();
        }
    }
}
