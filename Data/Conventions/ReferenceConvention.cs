﻿// <copyright file="ReferenceConvention.cs" company="GoGoId">
// GoGoId copyright, 2013
// </copyright>
// <author>Natalie Vegerina nvegerina@infostroy.com.ua</author>

namespace Data.Conventions
{
    using System;
    using System.Collections.Generic;
    using FluentNHibernate.Conventions.Instances;
    using FluentNHibernate.Mapping;
    using FluentNHibernate.Conventions;

    public class ReferenceConvention : IReferenceConvention
    {
        public void Apply(IManyToOneInstance instance)
        {
            instance.Cascade.None();
            instance.LazyLoad(Laziness.Proxy);
            instance.Column(instance.Property.Name + "Id");
        }
    }
}
