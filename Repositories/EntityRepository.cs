﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain;
using NHibernate;

namespace Repositories
{
    public class EntityRepository<T>
        where T : Entity
    {
        public EntityRepository(ISession session)
        {
            this.Session = session;
        }

        public ISession Session { get; private set; }

        public IList<T> Load()
        {
            return this.Session.QueryOver<T>().List();
        }

        public void Save(T entity) 
        {
            this.Session.Save(entity);
        }

        public T Load(int id) 
        {
            return this.Session.QueryOver<T>()
                .Where(x => id == x.Id.Value).SingleOrDefault();
        }
    }
}
