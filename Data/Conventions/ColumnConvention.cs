﻿// <copyright file="ColumnConvention.cs" company="GoGoId">
// GoGoId copyright, 2013
// </copyright>
// <author>Natalie Vegerina nvegerina@infostroy.com.ua</author>

namespace Data.Conventions
{
    using System.Collections.Generic;
    using FluentNHibernate.Conventions;
    using FluentNHibernate.Conventions.Instances;
    using System;

    public class ColumnConvention : IPropertyConvention
    {
        private static List<string> types = new List<string>()
        {
            "Type",
            "Order",
            "Text",
            "Key"
        };

        public void Apply(IPropertyInstance instance)
        {
            if (types.Contains(instance.Name))
            {

                instance.Column(string.Format("`{0}`", Inflector.Inflector.Underscore(instance.Name)));
            }
            else
            {
                instance.Column(Inflector.Inflector.Underscore(instance.Name));
            }

            Type memberType = instance.Property.PropertyType;
            if (memberType.BaseType == typeof(Nullable<>))
            {
                instance.Nullable();
            }
            else if (memberType.IsPrimitive)
            {
                instance.Not.Nullable();
            }
            else if (memberType.IsEnum)
            {
                instance.Not.Nullable();
            }
            else
            {
                /* object[] attrs = instance.Property.MemberInfo.GetCustomAttributes(typeof(NotNull), true);
                 if (attrs != null && attrs.Length > 0)
                 {
                     if ((attrs[0] as NotNull).IsNotNull)
                     {
                         instance.Not.Nullable();
                     }
                     else
                     {
                         instance.Nullable();
                     }
                 }
                 else
                 {
                     instance.Nullable();
                 }*/
            }

            /*if (instance.Property.MemberInfo.IsDefined(typeof(LazyLoadAttribute), true))
            {
                instance.LazyLoad();
            }

            object[] attributes = instance.Property.MemberInfo.GetCustomAttributes(typeof(MaxLengthAttribute), true);
            if (attributes != null && attributes.Length > 0)
            {
                instance.Length((attributes[0] as MaxLengthAttribute).Value);
            }*/
        }
    }
}
