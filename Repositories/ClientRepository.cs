﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain;
using NHibernate;

namespace Repositories
{
    public class UserRepository : EntityRepository<User>
    {
        public UserRepository(ISession session)
            : base(session)
        {
        }
    }
}
